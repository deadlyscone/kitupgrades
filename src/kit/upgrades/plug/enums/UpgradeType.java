package kit.upgrades.plug.enums;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kit.upgrades.plug.files.KitDataManager;

public enum UpgradeType{
	HEALTH, MANA, SWORD, BOW;

	@SuppressWarnings("deprecation")
	public static boolean isConfigItem(UpgradeType type, Player player) {
		for(Slot slot : Slot.values()){
			ItemStack currentItem = player.getItemInHand();
			String[] data = KitDataManager.configSlotItemsMap.get(WorldType.DEFAULT).get(slot).split(",");
			Integer id = data[0].equalsIgnoreCase("sword") ? UpgradeLevel.getSword(player).getTypeId(): Integer.valueOf(data[0]);
			Integer amt = Integer.valueOf(data[2]);
			short meta = Short.valueOf(data[1]);
			String name = ChatColor.translateAlternateColorCodes('&', data[3]);
			ItemStack item = new ItemStack(id, amt, meta);
			ItemMeta im = item.getItemMeta();
			
			if (!name.equals("-")){
				im.setDisplayName(name);
				item.setItemMeta(im);
			}
			if(item.equals(currentItem)){
				return true;
			}
		}
		return false;
		
	}
}
