package kit.upgrades.plug.upgrade;

import java.util.ArrayList;
import java.util.List;

import kit.upgrades.plug.enums.GUIConfigType;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.events.HealthManager;
import kit.upgrades.plug.events.WorldEvents;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class UpgradeGUI implements Listener {
	
	private static String guiName = "default";

	public UpgradeGUI(KitUpgrades plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	

	@SuppressWarnings("deprecation")
	public static void displayGUI(Player player) {
		String playerPoints = String.valueOf(UpgradeManager.getUpgradePoints(player.getUniqueId()));
		guiName = playerPoints.equals("0") ? (ChatColor.DARK_GRAY + "Current Points: " + ChatColor.RED + playerPoints) : (ChatColor.DARK_GRAY + 
				"Current Points: " + ChatColor.DARK_GREEN + playerPoints);
		Inventory gui = Bukkit.getServer().createInventory(null, 9, guiName);

		//Fill in inventory first;
		for(int i = 0; i < gui.getSize(); i++){
			String[] fillData = KitDataManager.configGUIMap.get(GUIConfigType.FillBlock).split(",");
			int fillTypeID = Integer.valueOf(fillData[0]);
			short fillTypeMeta = Short.valueOf(fillData[1]);
			ItemStack fill = new ItemStack(fillTypeID, 1, fillTypeMeta);
			ItemMeta fillMeta = fill.getItemMeta();
			
			fillMeta.setDisplayName(ChatColor.GRAY + "");
			fill.setItemMeta(fillMeta);
			gui.setItem(i, fill);
		}
		for (UpgradeType type : UpgradeType.values()) {
			int currentLevel = UpgradeLevel.convertLevel(KitDataManager.playerLevelMap.get(player.getUniqueId()).get(type));
			List<String> lores = new ArrayList<String>();
			String[] itemData = KitDataManager.configItemsGUIMap.get(type).split(",");
			int itemTypeID = Integer.valueOf(itemData[0]);
			short itemTypeMeta = Short.valueOf(itemData[1]);
			ItemStack item = new ItemStack(itemTypeID, 1, itemTypeMeta);
			ItemMeta im = item.getItemMeta();
			int slot = getUpgradeTypeSlot(type);
			String upgradeName = new String("Upgrade " + type.toString().charAt(0) + type.toString().substring(1, type.toString().length()).toLowerCase());
			
			lores.add(ChatColor.GRAY + "Current Level: " + ChatColor.GREEN + currentLevel);
			im.setDisplayName(ChatColor.AQUA + upgradeName);
			im.setLore(lores);
			item.setItemMeta(im);
			
			gui.setItem(slot, item);
		}
		//open the inventory
		player.openInventory(gui);
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onGUIClick(InventoryClickEvent e) {
			
		if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
				e.getInventory().getName().equals(guiName) && e.getView().getItem(e.getSlot()).equals(e.getCurrentItem())){
			Player player = e.getWhoClicked() instanceof Player ? (Player) e.getWhoClicked() : null;
			if(player == null){return;}
			
			e.setCancelled(true);
			int playerPoints = UpgradeManager.getUpgradePoints(player.getUniqueId());
			boolean playerHasPoints = playerPoints > 0;
			
			if(playerHasPoints){
				UpgradeType upType = getUpgradeType(e.getCurrentItem());
				UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(player.getUniqueId()).get(upType);
				if(upLevel == null){return;}
				int currentLevel = UpgradeLevel.convertLevel(upLevel);
				
				if(currentLevel < 5){
					
					// Set the new level
					KitDataManager.playerLevelMap.get(player.getUniqueId()).put(upType, UpgradeLevel.convertLevel(currentLevel+1));
					
					//remove 1 point from the players total points
					KitDataManager.playerPointDataMap.put(player.getUniqueId(), (playerPoints-1));
					
					/*
					 * Set the health scale.
					 */
					if(upType == UpgradeType.HEALTH){
						HealthManager.setHealthScale(player);
					}
					switch(upType){
					case HEALTH:
						HealthManager.setHealthScale(player);
						break;
					case SWORD:
						WorldEvents.setDefaultSlotItem(player);
						break;
					default:
						break;
					}
					displayGUI(player);
				}else{
					player.closeInventory();
					player.sendMessage(ChatColor.RED + "[KitUpgrades] This skill is already maxed.");
				}
				
			}else{
				player.closeInventory();
				player.getInventory().remove(UpgradeManager.getTriggerItem());
				player.sendMessage(ChatColor.RED + "[KitUpgrades] You do not have the points required to perform this action.");
			}
			
			
			
		}else if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
				e.getInventory().getName().equals(guiName) && !e.getView().getItem(e.getSlot()).equals(e.getCurrentItem())){
				e.setCancelled(true);
			}
	}
	
	@SuppressWarnings("deprecation")
	private UpgradeType getUpgradeType(ItemStack item){
		for(UpgradeType type : UpgradeType.values()){
			String[] itemData = KitDataManager.configItemsGUIMap.get(type).split(",");
			int itemTypeID = Integer.valueOf(itemData[0]);
			short itemTypeMeta = Short.valueOf(itemData[1]);
			//ItemStack item = new ItemStack(itemTypeID, 1, itemTypeMeta);
			if(item.getTypeId() == itemTypeID && item.getDurability() == itemTypeMeta){
				return type;
			}
		}
		return null;
	}
	
	
	
	/*@SuppressWarnings("deprecation")
	private static ItemStack getUpgradeTypeItem(UpgradeType type){
		switch(type){
		case BOW:
			return new ItemStack(261, 1);
		case HEALTH:
			return new ItemStack(351, 1, (short)1);
		case MANA:
			return new ItemStack(351, 1, (short)4);
		case SWORD:
			return new ItemStack(267, 1);
		default:
			return null;
		
		}
		
	}
	*/
	private static Integer getUpgradeTypeSlot(UpgradeType type){
		switch(type){
		case BOW:
			return 3;
		case HEALTH:
			return 5;
		case MANA:
			return 8;
		case SWORD:
			return 0;
		default:
			return 0;
		
		}
		
	}
}
