package kit.upgrades.plug.upgrade;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import kit.upgrades.plug.enums.GUIConfigType;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.files.KitDataManager;

public class UpgradeManager {
	
	
	public static void addUpgradePoints(UUID uid, Integer points){
		if (KitDataManager.playerPointDataMap == null){
			KitDataManager.playerPointDataMap = new HashMap<UUID, Integer>();
			KitDataManager.playerPointDataMap.put(uid, KitDataManager.defaultPoints);
		}
		if (!KitDataManager.playerPointDataMap.containsKey(uid)){
			KitDataManager.playerPointDataMap.put(uid, KitDataManager.defaultPoints);
		}
		if (!KitDataManager.playerPointDataMap.containsKey(uid) && KitDataManager.playerPointDataMap.get(uid) == null){
			KitDataManager.playerPointDataMap.put(uid, KitDataManager.defaultPoints);
		}
		if(KitDataManager.playerPointDataMap.containsKey(uid) && (KitDataManager.playerPointDataMap.get(uid)) + points < 11){
			int currentPoints = KitDataManager.playerPointDataMap.get(uid);
			int f = (points + currentPoints);
			KitDataManager.playerPointDataMap.put(uid, f);
		}
	}
	
	public static Integer getUpgradePoints(UUID uid){
		return KitDataManager.playerPointDataMap.get(uid);
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack getTriggerItem(){
		String[] triggerData = KitDataManager.configGUIMap.get(GUIConfigType.InventoryTrigger).split(",");
		int triggerTypeID = Integer.valueOf(triggerData[0]);
		short triggerTypeMeta = Short.valueOf(triggerData[1]);
		String triggerDisplayName = ChatColor.translateAlternateColorCodes('&', triggerData[2]);
		ItemStack triggerItem = new ItemStack(triggerTypeID, 1, triggerTypeMeta);
		ItemMeta im = triggerItem.getItemMeta();
		im.setDisplayName(triggerDisplayName);
		triggerItem.setItemMeta(im);
		return triggerItem;
	}
	
	private static int i = 1800;
	public static void runAutoRemovePoints(Plugin plugin){
	    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

		scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
	        @Override
	        public void run() {
	        	if(i == 0){
	        		KitDataManager.playerPointDataMap = new HashMap<UUID, Integer>();
	        		HashMap<UpgradeType, UpgradeLevel> map = new HashMap<UpgradeType, UpgradeLevel>();
	        		for(UpgradeType type : UpgradeType.values()){
	        			map.put(type, UpgradeLevel.Level1);
	        		}
	        		Iterator<Entry<UUID, HashMap<UpgradeType, UpgradeLevel>>> it = KitDataManager.playerLevelMap.entrySet().iterator();
	        	    while (it.hasNext()) {
	        	        Map.Entry<UUID, HashMap<UpgradeType, UpgradeLevel>> pair = (Map.Entry<UUID, HashMap<UpgradeType, UpgradeLevel>>)it.next();
	        	        pair.setValue(map);
	        	    }
                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Reset.");

                    i = 1800;

                }else if(i == 1){

                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Resetting in" + ChatColor.GREEN + " 1");

                }else if(i == 2){

                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Resetting in" + ChatColor.GREEN + " 2");

                }else if(i == 3){

                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Resetting in" + ChatColor.GREEN +  " 3");

                }else if(i == 4){

                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Resetting in" + ChatColor.GREEN + " 4");

                }else if(i == 5){

                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Resetting in" + ChatColor.GREEN + " 5");

                }else if(i == 60){
                    Bukkit.broadcastMessage(ChatColor.GRAY + "Points/Levels Resetting in" + ChatColor.GREEN + " 1 Minute");
	        	}
	        	i--;
	        }
	    }, 0L, 20L);
	}

}
