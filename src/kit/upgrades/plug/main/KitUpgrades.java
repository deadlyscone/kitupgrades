package kit.upgrades.plug.main;

import kit.upgrades.plug.events.CombatEvents;
import kit.upgrades.plug.events.CommandManager;
import kit.upgrades.plug.events.DurabilityModifier;
import kit.upgrades.plug.events.HealthManager;
import kit.upgrades.plug.events.JoinEvents;
import kit.upgrades.plug.events.RelicManager;
import kit.upgrades.plug.events.StaminaManager;
import kit.upgrades.plug.events.UpgradeEvents;
import kit.upgrades.plug.events.WorldEvents;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.upgrade.UpgradeGUI;
import kit.upgrades.plug.upgrade.UpgradeManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class KitUpgrades extends JavaPlugin{
	
	@Override
	public void onEnable() {
		RegisterClassEvents();
		//ConfigParser.scanFiles(true);
		KitDataManager.loadAllData(true);
		StaminaManager.runStaminaRegeneration();
		HealthManager.runHealthRegeneration();
		//RelicManager.runRelicDuplicationVerifier();
		UpgradeManager.runAutoRemovePoints(this);
	}

	@Override
	public void onDisable() {
		KitDataManager.saveAllData();
	}
	
	private void RegisterClassEvents() {
		new UpgradeGUI(this);
		new UpgradeEvents(this);
		new CombatEvents(this);
		new JoinEvents(this);
		new HealthManager(this);
		new StaminaManager(this);
		new WorldEvents(this);
		new DurabilityModifier(this);
		new RelicManager(this);
		this.getCommand("ku").setExecutor(new CommandManager(this));
		this.getCommand("kupgrades").setExecutor(new CommandManager(this));
		
	}
	
	public static void reload(){
		System.out.println("[KitUpgrades] Preparing to reload...");
		KitDataManager.saveAllData();
		//ConfigParser.scanFiles(true);
		KitDataManager.loadAllData(true);
		for(Player p : Bukkit.getServer().getOnlinePlayers()){
			HealthManager.setHealthScale(p);
			WorldEvents.setDefaultSlotItem(p);
		}
		System.out.println("[KitUpgrades] Reloaded!");
	}
}
