package kit.upgrades.plug.events;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.files.UUIDFetcher;
import kit.upgrades.plug.main.KitUpgrades;
import kit.upgrades.plug.upgrade.UpgradeGUI;
import kit.upgrades.plug.upgrade.UpgradeManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class CommandManager implements CommandExecutor{
	private Plugin plugin;
 public CommandManager(KitUpgrades plugin) {
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if ((label.equalsIgnoreCase("ku") || label.equalsIgnoreCase("kupgrades"))){
			
			if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
			if (sender instanceof Player && sender.hasPermission("kitupgrades.admin")){
				KitUpgrades.reload();
				sender.sendMessage(ChatColor.RED + "[KitUpgrades] Reloaded.");
				return true;
			}else if (!(sender instanceof Player)){
				KitUpgrades.reload();
				return true;
			}
		  }
			if (sender instanceof Player && sender.hasPermission("kitupgrades.admin")){
				Player player = (Player)sender;
				if((args.length == 2 || args.length == 3 || args.length == 4) && args[0].equalsIgnoreCase("give")){
					if(args.length == 2 && args[1].equalsIgnoreCase("trigger")){
						player.getInventory().addItem(UpgradeManager.getTriggerItem());
						return true;
						
					}else if((args.length == 3 || args.length == 4) && args[1].equalsIgnoreCase("relic")){
						Relic requestedRelic = null;
						if(args[2].equalsIgnoreCase("health")){
							requestedRelic = Relic.HEALTH;
						}else if(args[2].equalsIgnoreCase("mana") || args.length == 3 && args[2].equalsIgnoreCase("stamina")){
							requestedRelic = Relic.STAMINA;
						}else if(args[2].equalsIgnoreCase("strength")){
							requestedRelic = Relic.STRENGTH;
						}
						
						/*
						 * If the command was called with a player name.
						 */
						if(requestedRelic != null){
							
							if (args.length == 4){
								
								UUIDFetcher getUUID = new UUIDFetcher(args[3].toString(), false);
								UUID playerUID = null;
								try {
									playerUID = getUUID.getUUIDOf(args[3]);
								} catch (Exception e) {
									e.printStackTrace();
								}
								if(playerUID != null){
									
									boolean playerOnline = false;
									Player targetPlayer = null;
									for (Player p : Bukkit.getServer().getOnlinePlayers()){
										if (playerUID.equals(p.getUniqueId())){
											playerOnline = true;
											targetPlayer = p;
											break;
										}
									}
									
									if(playerOnline){
										Integer firstEmpty = targetPlayer.getInventory().firstEmpty();
										if (firstEmpty == -1) {
											targetPlayer.sendMessage("[KitUpgrades] Please empty out one inventory slot before you can receive your relic.");
											targetPlayer.sendMessage("[KitUpgrades] After you do so, please re-login");
										}else{
											
											ItemStack reqRelic = Relic.getRelicItem(requestedRelic);
											targetPlayer.getInventory().setItem(firstEmpty, reqRelic);
											return true;
										}
										
									}
									
									/*
									 * This falls through if the targetPlayer does not have an empty slot.
									 * OR
									 * if the player is not online
									 */
										List<Relic> dataMap = KitDataManager.playerBuyRelicMap.get(playerUID);
										
										if(dataMap == null){
											dataMap = new ArrayList<Relic>();
										}
										dataMap.add(requestedRelic);
										KitDataManager.playerBuyRelicMap.put(playerUID, dataMap);
										return true;
									
								}
								
							}else{
								Integer firstEmpty = player.getInventory().firstEmpty();
								if (firstEmpty == -1) {
									player.sendMessage("[KitUpgrades] Please empty out one inventory slot before you can receive your relic.");
									player.sendMessage("[KitUpgrades] After you do so, please re-login");
								}else{
									
									ItemStack reqRelic = Relic.getRelicItem(requestedRelic);
									player.getInventory().setItem(firstEmpty, reqRelic);
									return true;
								}
								return true;
							}
							
						}
						
					}
				}else if(args.length == 1 && args[0].equalsIgnoreCase("gui")){
					UpgradeGUI.displayGUI(player);
					return true;
				}
				
			}
			
		}
		
		if(sender instanceof Player){
			Player player = (Player) sender;
			player.sendMessage(ChatColor.DARK_AQUA+"_________________[KitUpgrades]_________________");
			player.sendMessage(ChatColor.AQUA + "Version: " + plugin.getDescription().getVersion());
			player.sendMessage(ChatColor.AQUA + "Created by: deadlyscone.");
			player.sendMessage(ChatColor.AQUA + "______________________________________________");
			if (sender.hasPermission("kitupgrades.admin")){
				player.sendMessage(ChatColor.GOLD + "---------------[Commands]---------------");
				player.sendMessage(ChatColor.RED + "/ku reload  -  " + ChatColor.GREEN + "Reloads the plugin data and config.");
				player.sendMessage(ChatColor.RED + "/ku give trigger  -  " + ChatColor.GREEN + "Gives the kit GUI upgrade item.");
				player.sendMessage(ChatColor.RED + "/ku give relic [( health, mana, strength ) { player }] -  " + ChatColor.GREEN + "Gives the specified relic item.");
				player.sendMessage(ChatColor.RED + "/ku gui  -  " + ChatColor.GREEN + "opens the upgrade gui.");
			}
		}else{
			System.out.println("[KitUpgrades] Commands: /ku reload");
		}
		
		return true;
	}

}
