package kit.upgrades.plug.events;

import kit.upgrades.plug.enums.GeneralConfigType;
import kit.upgrades.plug.enums.Slot;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.WorldType;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class WorldEvents implements Listener{
	
	public WorldEvents(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerChangeWorld(PlayerChangedWorldEvent e){
		String worldName = e.getPlayer().getWorld().getName();
		String cartelWorldName = KitDataManager.configGeneralMap.get(GeneralConfigType.CARTEL_WORLD);
		if(cartelWorldName != null && !cartelWorldName.equalsIgnoreCase("null") && worldName.equalsIgnoreCase(cartelWorldName)){
			Player player = e.getPlayer();
			setDefaultSlotItem(player);
		}else{
			Player player = e.getPlayer();
			setDefaultSlotItem(player);
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public static void setDefaultSlotItem(Player player){
		String worldName = player.getWorld().getName();
		String cartelWorldName = KitDataManager.configGeneralMap.get(GeneralConfigType.CARTEL_WORLD);
		//ItemStack slot3Item = player.getInventory().getItem(2);
		if(cartelWorldName != null && !cartelWorldName.equalsIgnoreCase("null") && worldName.equalsIgnoreCase(cartelWorldName)){
			/*
			 * Player is in the cartel world
			 */
			for(Slot slot : Slot.values()){
				if(slot == Slot.SLOT_1){
				String[] data = KitDataManager.configSlotItemsMap.get(WorldType.CARTEL).get(slot).split(",");
				Integer id = data[0].equalsIgnoreCase("sword") ? UpgradeLevel.getSword(player).getTypeId(): Integer.valueOf(data[0]);
				Integer amt = Integer.valueOf(data[2]);
				short meta = Short.valueOf(data[1]);
				String name = ChatColor.translateAlternateColorCodes('&', data[3]);
				ItemStack item = new ItemStack(id, amt, meta);
				ItemMeta im = item.getItemMeta();
				Integer index = Slot.convertSlot(slot);
				if (!name.equals("-")){
					im.setDisplayName(name);
					item.setItemMeta(im);
				}
				
				
				if(id == 261){
					item.addEnchantment(Enchantment.ARROW_INFINITE, 1);
				}
				/*
				 * Set the item
				 */
				player.getInventory().setItem(index, item);
				}else{
					Integer s4 = Slot.convertSlot(slot);
					if(player.getInventory().getItem(s4) != null && !player.getInventory().getItem(s4).getType().equals(Material.AIR)){
						player.getInventory().setItem(s4, new ItemStack(Material.AIR));
					}
				}
			}
			
		}else{
			/*
			 *  Player is in a default world or cartelworld is null
			 */
			for(Slot slot : Slot.values()){
					String[] data = KitDataManager.configSlotItemsMap.get(WorldType.DEFAULT).get(slot).split(",");
					Integer id = data[0].equalsIgnoreCase("sword") ? UpgradeLevel.getSword(player).getTypeId(): Integer.valueOf(data[0]);
					Integer amt = Integer.valueOf(data[2]);
					short meta = Short.valueOf(data[1]);
					String name = ChatColor.translateAlternateColorCodes('&', data[3]);
					ItemStack item = new ItemStack(id, amt, meta);
					ItemMeta im = item.getItemMeta();
					Integer index = Slot.convertSlot(slot);
					
					if (!name.equals("-")){
						im.setDisplayName(name);
						item.setItemMeta(im);
					}
					if(id == 261){
						item.addEnchantment(Enchantment.ARROW_INFINITE, 1);
					}
					
					/*
					 * Checks to see if a player has a personal item in the default slot
					 */
					if(slot != Slot.SLOT_1){
						
						Integer slotID = Slot.convertSlot(slot);
						int first = player.getInventory().firstEmpty();
						
						if (player.getInventory().getItem(slotID) != null && player.getInventory().getItem(slotID).getType() != Material.AIR && 
								!player.getInventory().getItem(slotID).equals(item)){
							ItemStack oldItem = player.getInventory().getItem(slotID);
							if(first == -1){
								player.getWorld().dropItemNaturally(player.getLocation(), oldItem);
							}else{
								System.out.println(oldItem);
								player.getInventory().setItem(first, oldItem);
							}
						}
					}
					
					
					/*
					 * Set the item
					 */
					player.getInventory().setItem(index, item);
			}
			
		}
	}
}
