package kit.upgrades.plug.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinEvents implements Listener{
	
	public JoinEvents(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e){
		
		/*
		 * Check if our player is already in the point map
		 */
		if (!KitDataManager.playerPointDataMap.containsKey(e.getPlayer().getUniqueId())){
			KitDataManager.playerPointDataMap.put(e.getPlayer().getUniqueId(), KitDataManager.defaultPoints);
		}
		if (!KitDataManager.playerDamageMap.containsKey(e.getPlayer().getUniqueId())){
			HashMap<UUID, Double> map = new HashMap<UUID, Double>();
			KitDataManager.playerDamageMap.put(e.getPlayer().getUniqueId(), map);
		}
		if (!KitDataManager.playerLevelMap.containsKey(e.getPlayer().getUniqueId())){
			HashMap<UpgradeType, UpgradeLevel> map = new HashMap<UpgradeType, UpgradeLevel>();
			for(UpgradeType type : UpgradeType.values()){
				map.put(type, UpgradeLevel.Level1);
			}
			KitDataManager.playerLevelMap.put(e.getPlayer().getUniqueId(), map);
			
			/*
			 * Set the default mana for our new player.
			 */
			StaminaManager.setStamina(e.getPlayer(), StaminaManager.getMaxStamina(e.getPlayer()));
					
			
		}
		
		/*
		 * Set Health
		 */
		HealthManager.setHealthScale(e.getPlayer());
		
		/*
		 * Set default Items
		 */
		WorldEvents.setDefaultSlotItem(e.getPlayer());
		
		/*
		 * Check if player bought a relic.
		 */

		if (KitDataManager.playerBuyRelicMap.containsKey(e.getPlayer().getUniqueId())) {
			Player player = e.getPlayer();
			Integer firstEmpty;
			List<Relic> memListRelics = KitDataManager.playerBuyRelicMap.get(player.getUniqueId());
			List<Relic> remainingRelics = new ArrayList<Relic>(memListRelics);
			for (Relic relic : memListRelics) {
				firstEmpty = player.getInventory().firstEmpty();
				if (firstEmpty == -1) {
					player.sendMessage("[KitUpgrades] Please empty out " + memListRelics.size() + " slot(s) before you can receive your relic(s).");
					player.sendMessage("[KitUpgrades] After you do so, please re-login");
				} else {
					player.getInventory().setItem(firstEmpty, Relic.getRelicItem(relic));
					remainingRelics.remove(relic);
				}
			}
			KitDataManager.playerBuyRelicMap.put(player.getUniqueId(), remainingRelics);
		}

	}
	public static void resetPlayerLevels(Player player){
	HashMap<UpgradeType, UpgradeLevel> map = new HashMap<UpgradeType, UpgradeLevel>();
	for(UpgradeType type : UpgradeType.values()){
		map.put(type, UpgradeLevel.Level1);
	}
	KitDataManager.playerLevelMap.put(player.getUniqueId(), map);
	StaminaManager.setStamina(player.getPlayer(), StaminaManager.getMaxStamina(player));
	HealthManager.setHealthScale(player.getPlayer());
	}
}
