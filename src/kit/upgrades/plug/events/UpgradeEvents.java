package kit.upgrades.plug.events;

import kit.upgrades.plug.enums.GeneralConfigType;
import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;
import kit.upgrades.plug.upgrade.UpgradeGUI;
import kit.upgrades.plug.upgrade.UpgradeManager;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class UpgradeEvents implements Listener{
	
	public UpgradeEvents(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerRequestGUI(PlayerInteractEvent e){
		
		if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR){
			Player player = e.getPlayer();
			ItemStack triggerItem = UpgradeManager.getTriggerItem();
			
			if(player.getItemInHand().equals(triggerItem)){
				e.setCancelled(true);
				UpgradeGUI.displayGUI(player);
			}
		}
		
	}
	@EventHandler
	public void onSlotItemClick(InventoryClickEvent e){
		
		if(e.getWhoClicked() instanceof Player){
			Player player = (Player)e.getWhoClicked();
			String worldName = player.getWorld().getName();
			String cartelWorldName = KitDataManager.configGeneralMap.get(GeneralConfigType.CARTEL_WORLD);
			Integer maxSlot = !worldName.equalsIgnoreCase(cartelWorldName) ? 3 : 1;
			boolean isRelic = false;
			
			if (e.getHotbarButton() > -1 && e.getHotbarButton() < maxSlot){
				e.setCancelled(true);
			}
			if(e.getSlotType() == SlotType.QUICKBAR && e.getSlot() > -1 && e.getSlot() < maxSlot){
				e.setCancelled(true);
			}
			
			for(Relic relic : Relic.values()){
				ItemStack relItem = Relic.getRelicItem(relic);
				isRelic = (e.getCursor() != null && e.getCursor().equals(relItem)) || (e.getCurrentItem() != null && e.getCurrentItem().equals(relItem));
				if(isRelic){
					break;
				}
			}
			
			if(isRelic){
				if(e.getInventory().getType() == InventoryType.ENDER_CHEST && e.getSlotType() == SlotType.OUTSIDE){
					e.setCancelled(true);
				}else if (e.getInventory().getType() == InventoryType.CRAFTING && e.getSlotType() == SlotType.OUTSIDE){
					e.setCancelled(true);
				}else if(e.getInventory().getType() != InventoryType.ENDER_CHEST && e.getInventory().getType() != InventoryType.CRAFTING){
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler
	public void onPlayerTossItem(PlayerDropItemEvent e){
		int slotHeld = e.getPlayer().getInventory().getHeldItemSlot();
		Player player = e.getPlayer();
		
		String worldName = player.getWorld().getName();
		String cartelWorldName = KitDataManager.configGeneralMap.get(GeneralConfigType.CARTEL_WORLD);
		int maxSlot = !worldName.equalsIgnoreCase(cartelWorldName) ? 2 : 0;
		boolean isRelic = false;
		
		for(Relic relic : Relic.values()){
			ItemStack relItem = Relic.getRelicItem(relic);
			isRelic = (e.getItemDrop().getItemStack().equals(relItem));
			if(isRelic){
				break;
			}
		}
		
		if((slotHeld > -1 && slotHeld <= maxSlot) || isRelic){
			e.setCancelled(true);
		}
		
	}
}
