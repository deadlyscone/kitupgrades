package kit.upgrades.plug.events;

import java.util.ArrayList;
import java.util.List;

import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class RelicManager implements Listener{
	static Plugin plugin;
	
	public RelicManager(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		RelicManager.plugin = plugin;
	}

	public static List<Relic> getPlayerRelics(Player player){
		List<Relic> relics = new ArrayList<Relic>();
		
		for (int i = 0; i < player.getInventory().getSize(); i++){
			for(Relic relic : Relic.values()){
				if(player.getInventory().getItem(i) != null){
					ItemStack relItem = Relic.getRelicItem(relic);
					ItemStack playerItem = player.getInventory().getItem(i);
					if(relItem.equals(playerItem)){
						relics.add(relic);
					}
				}
			}
		}
		return relics;
	}
	/*
	public static void runRelicDuplicationVerifier(){
	    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

		scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
	        @Override
	        public void run() {
	        	for (Player p : Bukkit.getServer().getOnlinePlayers()){
	        		if (!p.isDead()){
	        			for(int i = 0; i < p.getInventory().getSize(); i++){
	        				for(Relic relic : Relic.values()){
	        					if(p.getInventory().getItem(i) != null){
		        					ItemStack pItem = p.getInventory().getItem(i);
		        					ItemStack relItem = Relic.getRelicItem(relic);
		        					if(pItem.getType().equals(relItem.getType()) && pItem.getDurability() == relItem.getDurability() && 
		        							pItem.getItemMeta() != null && pItem.getItemMeta().getDisplayName() != null && 
		        							pItem.getItemMeta().getDisplayName().equals(relItem.getItemMeta().getDisplayName()) && pItem.getAmount() > 1){
		        						p.getInventory().getItem(i).setAmount(1);
		        					}
		        				}
	        				}
	        				
	        			}
		        		
	        		}
	        	}
	        }
	    }, 0L, 59L);
	}
	*/
}
