package kit.upgrades.plug.events;

import java.util.List;
import java.util.UUID;

import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class StaminaManager implements Listener{
	
	private static Plugin plugin;
	public StaminaManager(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		StaminaManager.plugin = plugin;
	}

	@EventHandler
	public void onPlayerStarvation(EntityDamageEvent e){
		if (e.getCause() == DamageCause.STARVATION){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onStanimaChange(FoodLevelChangeEvent e){
			e.setCancelled(true);
		
	}
	
	public static void subtractStamina(Player player, Integer amt){
		Integer curMana = getStamina(player);
		if((curMana - amt) < 0){
			player.setFoodLevel(0);
		}else{
			player.setFoodLevel(curMana - amt);
			KitDataManager.playerCurrentMana.put(player.getUniqueId(), amt);
		}
	}
	
	public static void addStamina(Player player, Integer amt){
		Integer curMana = getStamina(player);
		int max = getMaxStamina(player);
		
		if((curMana + amt) > max){
			player.setFoodLevel(max);
		}else{
			player.setFoodLevel(curMana + amt);
		}
	}
	
	public static void setStamina(Player player, Integer amt){
		player.setFoodLevel(amt);
		KitDataManager.playerCurrentMana.put(player.getUniqueId(), amt);
	}
	
	public static Integer getStamina(Player player){
		return player.getFoodLevel();
	}
	
	public static Integer getMaxStamina(Player player){
		UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(player.getUniqueId()).get(UpgradeType.MANA);
		Integer amt = Integer.valueOf(KitDataManager.configLevelsMap.get(UpgradeType.MANA).get(upLevel).split(",")[0]);
		return amt;
	}
	
	public static void runStaminaRegeneration(){
	    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

		scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
	        @Override
	        public void run() {
	        	for (Player p : Bukkit.getServer().getOnlinePlayers()){
	        		if (!p.isDead()){
	        			UUID uid = p.getUniqueId();
		        		UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(uid).get(UpgradeType.MANA);
		        		Integer amt = Integer.valueOf(KitDataManager.configLevelsMap.get(UpgradeType.MANA).get(upLevel).split(",")[1]);
		        		List<Relic> relics = RelicManager.getPlayerRelics(p);
	        			if(relics.contains(Relic.STAMINA)){
	        				Integer a = amt;
	        				amt = new Integer(a + Relic.getManaRegenAmount());
	        			}
		        		addStamina(p, amt);
		        		p.setSaturation(1f);
	        		}
	        	}
	        }
	    }, 0L, (long)40);
	}
	
}
