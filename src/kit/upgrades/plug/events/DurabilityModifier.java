package kit.upgrades.plug.events;

import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class DurabilityModifier implements Listener{
	
	public DurabilityModifier(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onItemDurabilityLoss(PlayerInteractEvent e){
		if(e.getItem() != null && isWeapon(e.getMaterial())){
			e.getItem().setDurability((short)0);
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerShootBow(EntityShootBowEvent e){
		if (e.getEntity() instanceof Player && !e.isCancelled()){
			Player player = (Player)e.getEntity();
			if(player.getItemInHand().getType() == Material.BOW){
				ItemStack bow = player.getItemInHand();
				bow.setDurability((short)0);
				player.setItemInHand(bow);
			}
		}
	}
	private boolean isWeapon(Material m){
		switch(m){
		case WOOD_SWORD:
		case STONE_SWORD:
		case IRON_SWORD:
		case GOLD_SWORD:
		case DIAMOND_SWORD:
			return true;
		default:
			return false;
		}
	}
}
